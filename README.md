**Liste contenant l'intégralité des commandes et des commentaires associés pour lancer la plateforme**

`docker system prune`

- [ ] Créer un network nommé PONT qui me permettra de connecter mes containers les uns aux autres 

`docker network create PONT`

- [ ] Créer un container pour mon SGBDR nommé BDD – que je vais rattacher à un volume pour garantir la persistance des données, lier à mon network PONT et configurer à l’aide de variables d’environnement

`docker run -d --name MYBDD --network PONT -v ./data:/var/lib/mysql --env MARIADB_USER=WORDPRESS --env MARIADB_PASSWORD=BDD_PWD1 --env MARIADB_DATABASE=DATABASE_WORDPRESS --env MARIADB_ROOT_PASSWORD=BDD_PWD2 mariadb:11.2.2`

- [ ] Télécharger la dernière version de wordpress et placer le dossier wordpress dans mon répertoire courant

- [ ] Créer un dossier nommé nginx 

`mkdir nginx`

- [ ] Créer un container SCRIPT que je rattache à un volume et y ajouter l’extension mysqli

`docker run -d --rm --name SCRIPT --network PONT -v ./wordpress:/var/www/html php:8.2-apache`

`docker exec SCRIPT bash -c “docker-php-ext-configure mysqli;docker-php-ext-install mysqli;docker-php-ext-enable mysqli;apachectl graceful”`

- [ ] Créer le fichier par défaut

`docker run --rm nginx:1.25 cat /etc/nginx/conf.d/default.conf > nginx/default.conf`

- [ ] Éditer le fichier par défaut 

`vim nginx/default.conf`

- [ ] Y ajouter le proxy_pass

```
	server {
		listen	    80;
		server_name    mywpserver;
}
	location / {
		proxy_pass http://SCRIPT:80;
   		proxy_set_header Host $host;
    	proxy_set_header X-Real-IP $remote-addr;
}
```


- [ ] Lancer un conteneur HTTP en configurant un serveur nginx à l’aide du proxy_pass que je lie au network PONT

`docker run -d --rm --name HTTP --network PONT -p 80:80 -v ./nginx/default.conf:/etc/nginx/conf.d/default.conf:ro nginx:1.25`

`docker ps -a`



